import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-padre',
  templateUrl: './padre.component.html',
  styleUrls: ['./padre.component.css']
})
export class PadreComponent implements OnInit {

  padreMensaje1 = 'Hola hijo necesito que hagas unos encargos';
  padreMensaje2 = 've al mercado para hacer las compras';
  padreMensaje3 = 'compra frutas, verduras , leche y pan';
  padreMensaje4 = 'pide fiado al carnicero';
  padreMensaje5 = 'Regresa a casa antes de medio dia'
  
  
  constructor() { }

  ngOnInit(): void {
  }

}
